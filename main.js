jQuery(document).ready(function()
{
	jQuery.fn.extend({
		insertAtCaret: function(myValue)
		{
		  	return this.each(function(i) 
		  	{
			    if (document.selection) {
			      //For browsers like Internet Explorer
			      this.focus();
			      var sel = document.selection.createRange();
			      sel.text = myValue;
			      this.focus();
			    }
			    else if (this.selectionStart || this.selectionStart == '0') {
			      //For browsers like Firefox and Webkit based
			      var startPos = this.selectionStart;
			      var endPos = this.selectionEnd;
			      var scrollTop = this.scrollTop;
			      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
			      this.focus();
			      this.selectionStart = startPos + myValue.length;
			      this.selectionEnd = startPos + myValue.length;
			      this.scrollTop = scrollTop;
			    } 
			    else {
			      this.value += myValue;
			      this.focus();
			    }
		  	});
		}
	});


	var addObserver = function()
	{
		// Observe the DOM for changes to the text editor
		var observer = new MutationObserver(function(mutations) 
		{
			mutations.forEach(function(mutation)
			{
				if (!mutation.addedNodes) return;

				for ( var i = 0; i < mutation.addedNodes.length; i++ ) 
				{
					var node = mutation.addedNodes[i];

					if ( jQuery(node).attr("id") == "description-form" ) 
					{
						addTextEditor( jQuery(node).find('#description-wiki-edit').children('textarea') );
						observer.disconnect();
					}

					if ( jQuery(node).attr("id") == "create-issue-dialog" )
					{
						setTimeout(function() 
						{
							addTextEditor( jQuery(node).find('#description-wiki-edit').children('textarea') );
						}, 1000);
					}

					if ( jQuery(node).attr("id") == "issuetype" || jQuery(node).attr("id") == "project" )
					{
						setTimeout(function() 
						{
							addTextEditor( jQuery(node).closest('form').find('#description-wiki-edit').children('textarea') );
						}, 1000);						
					}
				}
			});
		});

		observer.observe( document.body, {
			childList: true,
			subtree: true,
			attributes: false,
			characterData: false
		});
	}


	var addTextEditor = function( ta )
	{
		if ( !jQuery(".j-text-editor")[0] )
		{
			var prependEditor = function( ta )
			{
				// Build the text editor bar and then inject it after the description heading
				var svgs = '<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <defs> <symbol id="icon-list-unordered" viewBox="0 0 24 32"> <title>list-unordered</title> <path class="path1" d="M0 18h4v-4h-4v4zM0 10h4v-4h-4v4zM0 26h4v-4h-4v4zM8 18h16v-4h-16v4zM8 10h16v-4h-16v4zM8 26h16v-4h-16v4z"></path> </symbol> <symbol id="icon-list-ordered" viewBox="0 0 24 32"> <title>list-ordered</title> <path class="path1" d="M10 18h14v-4h-14v4zM10 26h14v-4h-14v4zM10 6v4h14v-4h-14zM2.469 14h2.438v-8h-1.125l-2.656 0.719v1.563l1.344-0.063v5.781zM5.906 20.438c0-1.125-0.375-2.438-3-2.438-1.031 0-2 0.188-2.594 0.5l0.031 2.063c0.656-0.313 1.313-0.469 2.094-0.469s1 0.344 1 0.875c0 0.813-0.938 1.813-3.438 3.5v1.563h6v-2.094l-2.844 0.063c1.531-0.938 2.719-2.063 2.719-3.531l0.031-0.031z"></path> </symbol> <symbol id="icon-images" viewBox="0 0 36 32"> <title>images</title> <path class="path1" d="M34 4h-2v-2c0-1.1-0.9-2-2-2h-28c-1.1 0-2 0.9-2 2v24c0 1.1 0.9 2 2 2h2v2c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-24c0-1.1-0.9-2-2-2zM4 6v20h-1.996c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v1.996h-24c-1.1 0-2 0.9-2 2v0zM34 29.996c-0.001 0.001-0.002 0.003-0.004 0.004h-27.993c-0.001-0.001-0.003-0.002-0.004-0.004v-23.993c0.001-0.001 0.002-0.003 0.004-0.004h27.993c0.001 0.001 0.003 0.002 0.004 0.004v23.993z"></path> <path class="path2" d="M30 11c0 1.657-1.343 3-3 3s-3-1.343-3-3 1.343-3 3-3 3 1.343 3 3z"></path> <path class="path3" d="M32 28h-24v-4l7-12 8 10h2l7-6z"></path> </symbol> <symbol id="icon-bubbles4" viewBox="0 0 36 32"> <title>bubbles4</title> <path class="path1" d="M15 4c-1.583 0-3.112 0.248-4.543 0.738-1.341 0.459-2.535 1.107-3.547 1.926-1.876 1.518-2.91 3.463-2.91 5.474 0 1.125 0.315 2.217 0.935 3.247 0.646 1.073 1.622 2.056 2.821 2.842 0.951 0.624 1.592 1.623 1.761 2.748 0.028 0.187 0.051 0.375 0.068 0.564 0.085-0.079 0.169-0.16 0.254-0.244 0.754-0.751 1.771-1.166 2.823-1.166 0.167 0 0.335 0.011 0.503 0.032 0.605 0.077 1.223 0.116 1.836 0.116 1.583 0 3.112-0.248 4.543-0.738 1.341-0.459 2.535-1.107 3.547-1.926 1.876-1.518 2.91-3.463 2.91-5.474s-1.033-3.956-2.91-5.474c-1.012-0.819-2.206-1.467-3.547-1.926-1.431-0.49-2.96-0.738-4.543-0.738zM15 0v0c8.284 0 15 5.435 15 12.139s-6.716 12.139-15 12.139c-0.796 0-1.576-0.051-2.339-0.147-3.222 3.209-6.943 3.785-10.661 3.869v-0.785c2.008-0.98 3.625-2.765 3.625-4.804 0-0.285-0.022-0.564-0.063-0.837-3.392-2.225-5.562-5.625-5.562-9.434 0-6.704 6.716-12.139 15-12.139zM31.125 27.209c0 1.748 1.135 3.278 2.875 4.118v0.673c-3.223-0.072-6.181-0.566-8.973-3.316-0.661 0.083-1.337 0.126-2.027 0.126-2.983 0-5.732-0.805-7.925-2.157 4.521-0.016 8.789-1.464 12.026-4.084 1.631-1.32 2.919-2.87 3.825-4.605 0.961-1.84 1.449-3.799 1.449-5.825 0-0.326-0.014-0.651-0.039-0.974 2.268 1.873 3.664 4.426 3.664 7.24 0 3.265-1.88 6.179-4.82 8.086-0.036 0.234-0.055 0.474-0.055 0.718z"></path> </symbol> <symbol id="icon-search" viewBox="0 0 32 32"> <title>search</title> <path class="path1" d="M31.008 27.231l-7.58-6.447c-0.784-0.705-1.622-1.029-2.299-0.998 1.789-2.096 2.87-4.815 2.87-7.787 0-6.627-5.373-12-12-12s-12 5.373-12 12 5.373 12 12 12c2.972 0 5.691-1.081 7.787-2.87-0.031 0.677 0.293 1.515 0.998 2.299l6.447 7.58c1.104 1.226 2.907 1.33 4.007 0.23s0.997-2.903-0.23-4.007zM12 20c-4.418 0-8-3.582-8-8s3.582-8 8-8 8 3.582 8 8-3.582 8-8 8z"></path> </symbol> <symbol id="icon-enlarge" viewBox="0 0 32 32"> <title>enlarge</title> <path class="path1" d="M32 0h-13l5 5-6 6 3 3 6-6 5 5z"></path> <path class="path2" d="M32 32v-13l-5 5-6-6-3 3 6 6-5 5z"></path> <path class="path3" d="M0 32h13l-5-5 6-6-3-3-6 6-5-5z"></path> <path class="path4" d="M0 0v13l5-5 6 6 3-3-6-6 5-5z"></path> </symbol> <symbol id="icon-shrink2" viewBox="0 0 32 32"> <title>shrink2</title> <path class="path1" d="M14 18v13l-5-5-6 6-3-3 6-6-5-5zM32 3l-6 6 5 5h-13v-13l5 5 6-6z"></path> </symbol> <symbol id="icon-link" viewBox="0 0 32 32"> <title>link</title> <path class="path1" d="M13.757 19.868c-0.416 0-0.832-0.159-1.149-0.476-2.973-2.973-2.973-7.81 0-10.783l6-6c1.44-1.44 3.355-2.233 5.392-2.233s3.951 0.793 5.392 2.233c2.973 2.973 2.973 7.81 0 10.783l-2.743 2.743c-0.635 0.635-1.663 0.635-2.298 0s-0.635-1.663 0-2.298l2.743-2.743c1.706-1.706 1.706-4.481 0-6.187-0.826-0.826-1.925-1.281-3.094-1.281s-2.267 0.455-3.094 1.281l-6 6c-1.706 1.706-1.706 4.481 0 6.187 0.635 0.635 0.635 1.663 0 2.298-0.317 0.317-0.733 0.476-1.149 0.476z"></path> <path class="path2" d="M8 31.625c-2.037 0-3.952-0.793-5.392-2.233-2.973-2.973-2.973-7.81 0-10.783l2.743-2.743c0.635-0.635 1.664-0.635 2.298 0s0.635 1.663 0 2.298l-2.743 2.743c-1.706 1.706-1.706 4.481 0 6.187 0.826 0.826 1.925 1.281 3.094 1.281s2.267-0.455 3.094-1.281l6-6c1.706-1.706 1.706-4.481 0-6.187-0.635-0.635-0.635-1.663 0-2.298s1.663-0.635 2.298 0c2.973 2.973 2.973 7.81 0 10.783l-6 6c-1.44 1.44-3.355 2.233-5.392 2.233z"></path> </symbol> <symbol id="icon-text-color" viewBox="0 0 32 32"> <title>text-color</title> <path class="path1" d="M10.063 26l1.8-6h8.274l1.8 6h3.551l-6-20h-6.976l-6 20h3.551zM14.863 10h2.274l1.8 6h-5.874l1.8-6z"></path> </symbol> </defs> </svg>';
				var jTextEditor = '<div class="j-text-editor">' + svgs + '<div class="button bold">B</div><div class="button italic">I</div><div class="button color"><svg class="icon icon-text-color"><use xlink:href="#icon-text-color"></use></svg></div><div class="button h1">H1</div><div class="button h2">H2</div><div class="button h3">H3</div><div class="button h4">H4</div><div class="button bullet-unordered"><svg class="icon icon-list-unordered"><use xlink:href="#icon-list-unordered"></use></svg></div><div class="button bullet-ordered"><svg class="icon icon-list-ordered"><use xlink:href="#icon-list-ordered"></use></svg></div><div class="button link"><svg class="icon icon-link"><use xlink:href="#icon-link"></use></svg></div><div class="jt-right"><div class="button images"><svg class="icon icon-images"><use xlink:href="#icon-images"></use></svg></div><div class="button comments"><svg class="icon icon-bubbles4"><use xlink:href="#icon-bubbles4"></use></svg></div><div class="button fullscreen"><svg class="icon icon-enlarge"><use xlink:href="#icon-enlarge"></use></svg></div></div></div>';
				
				ta.parent().prepend(jTextEditor);

				var $form = jQuery('.j-text-editor').closest('form');
				var $cancelButton = $form.find('.save-options button:last-child');
				var $submitButton = $form.find('.save-options button:first-child');
				var $createButton = $form.find('.buttons-container input');
				var $cancelButton2 = $form.find('.buttons-container a');
				var saveButtonActions = function()
				{
					$cancelButton.click(function()
					{
						jQuery('.j-text-editor').remove();
						jQuery('html').removeClass('je-prevent-overflow');
						jQuery('body').removeClass('full-screen-editor').removeClass('no-images').removeClass('no-comments');
						jQuery(this).closest('form').parent().addClass('editable-field');
						addObserver();
					});

					$submitButton.click(function()
					{
						jQuery('html').removeClass('je-prevent-overflow');
						jQuery('body').removeClass('full-screen-editor').removeClass('no-images').removeClass('no-comments');
						addObserver();
					});

					$createButton.click(function()
					{
						jQuery('html').removeClass('je-prevent-overflow');
						jQuery('body').removeClass('full-screen-editor').removeClass('no-images').removeClass('no-comments');
						addObserver();
					});

					$cancelButton2.click(function()
					{
						jQuery('html').removeClass('je-prevent-overflow');
						jQuery('body').removeClass('full-screen-editor').removeClass('no-images').removeClass('no-comments');
						addObserver();
					});
				}

				jQuery('body').addClass('jte-editor');
				$form.addClass('jte-form');
				saveButtonActions();
			}


			var formatContents = function( wrapper, selected, closingWrapper=false )
			{
				if ( ( typeof selected === "undefined" || !selected ) && ( wrapper == "*" || wrapper == '_' || wrapper == '{color:red}' || wrapper == '[') )
				{
					return;
				}

				if ( /\s+$/.test( selected ) ) 
				{
					selected = selected.trim();
					var hadSpace = true;
				}

				if ( closingWrapper == -1 )
				{
					var data = wrapper + selected;
				}
				else if ( closingWrapper == -2 )
				{
					var data = '';
					var split = selected.split("\n");

					jQuery.each( split, function( key, val ) 
					{
						data += wrapper + val + '\n';
					});
				}
				else if ( closingWrapper == false )
				{
					var data = wrapper + selected + wrapper;
				}
				else
				{
					var data = wrapper + selected + closingWrapper;
				}

				if ( hadSpace == true ) 
				{
					data = data + ' ';
				}

				jQuery('#description-wiki-edit textarea').insertAtCaret(data);

				delete data;
				delete wrapper;
				delete selected;
				delete closingWrapper;
			}


			var buttonMgmt = function()
			{
				// Get the current selected text
				var $buttons = jQuery('.j-text-editor .button.bold, .j-text-editor .button.italic, .j-text-editor .button.color, .j-text-editor .button.bullet-ordered, .j-text-editor .button.bullet-unordered, .j-text-editor .button.h1, .j-text-editor .button.h2, .j-text-editor .button.h3, .j-text-editor .button.h4, .j-text-editor .button.link');

				$buttons.mouseover(function()
				{
					window.wte = [];
					window.wte.selected = window.getSelection().toString();
				});


				// BOLD the selected item
				jQuery('.j-text-editor .button.bold').click(function()
				{
					formatContents('*', window.wte.selected);
					delete window.wte;
				});	

				// Italicize the selected item
				jQuery('.j-text-editor .button.italic').click(function()
				{
					formatContents('_', window.wte.selected);
					delete window.wte;
				});	

				// Color the selected item red
				jQuery('.j-text-editor .button.h1').click(function()
				{
					formatContents('h1. ', window.wte.selected, -1);
					delete window.wte;
				});

				// Color the selected item red
				jQuery('.j-text-editor .button.h2').click(function()
				{
					formatContents('h2. ', window.wte.selected, -1);
					delete window.wte;
				});

				// Color the selected item red
				jQuery('.j-text-editor .button.h3').click(function()
				{
					formatContents('h3. ', window.wte.selected, -1);
					delete window.wte;
				});

				// Color the selected item red
				jQuery('.j-text-editor .button.h4').click(function()
				{
					formatContents('h4. ', window.wte.selected, -1);
					delete window.wte;
				});

				// Color the selected item red
				jQuery('.j-text-editor .button.color').click(function()
				{
					formatContents('{color:red}', window.wte.selected, '{color}');
					delete window.wte;
				});	

				// Adds styling for ordered lists
				jQuery('.j-text-editor .button.bullet-ordered').click(function()
				{
						
					var string = window.wte.selected.toString();
					if ( string.startsWith("*") )
					{
						wrapper = '#';
					}
					else
					{
						wrapper = '# ';
					}

					var match = /\r|\n/.exec(window.wte.selected);
					if (match) {
						// Run all the functions for a multiple line item
						var str = window.wte.selected.toString();

						formatContents( wrapper, str, -2 );
						delete window.wte;
					}
					else
					{
						formatContents( wrapper, window.wte.selected, -1 );
						delete window.wte;
					}

				});	

				// Adds styling for unordered lists
				jQuery('.j-text-editor .button.bullet-unordered').click(function()
				{
					var string = window.wte.selected.toString();
					if ( string.startsWith("*") )
					{
						wrapper = '*';
					}
					else
					{
						wrapper = '* ';
					}

					var match = /\r|\n/.exec(window.wte.selected);
					if (match) {
						// Run all the functions for a multiple line item
						var str = window.wte.selected.toString();

						formatContents( wrapper, str, -2 );
						delete window.wte;
					}
					else
					{
						formatContents( wrapper, window.wte.selected, -1 );
						delete window.wte;
					}									
				});


				// Adds logic for link editor
				jQuery('.j-text-editor .button.link').click(function()
				{
					var url = prompt("What is the URL?", "");

					formatContents('[', window.wte.selected, '|' + url + ']');
					delete window.wte;
				});


				// Adds style for full screen editor
				jQuery('.j-text-editor .button.fullscreen').click(function()
				{
					var $form = jQuery('.j-text-editor').closest('form');
					var $cancelButton = $form.find('.save-options button:last-child');
					var $submitButton = $form.find('.save-options button:first-child');
					var $wikiEdit = $form.find('.wiki-edit');
					var $descriptionWikiEdit = $form.find('#description-wiki-edit');
					var $saveOptions = $form.find('.save-options');

					document.body.scrollTop = document.documentElement.scrollTop = 0;
					jQuery('body').toggleClass('full-screen-editor');
					jQuery('html').toggleClass('je-prevent-overflow');
					jQuery('.aui-icon.wiki-renderer-icon').hide();

					if ( $form.parent().hasClass('editable-field') )
					{
						$form.parent().removeClass('editable-field');
					}
					else
					{
						$form.parent().addClass('editable-field');
					}
					
					if ( !jQuery('.je-preview-button-text')[0] )
					{
						jQuery('#description-preview_link').append('<svg class="icon icon-search"><use xlink:href="#icon-search"></use></svg><span class="je-preview-button-text">Preview</span>');
					}

					jQuery('#description-preview_link').click(function()
					{
						document.body.scrollTop = document.documentElement.scrollTop = 0;

						var $previewText = jQuery( '.je-preview-button-text');

						if ( $previewText.text() == 'Preview' )
						{
							$previewText.text('Edit');
							$previewText.parent().addClass('je-bgc-navy').addClass('je-c-white');
						}
						else if ( $previewText.text() == 'Edit' )
						{
							$previewText.text('Preview');
							$previewText.parent().removeClass('je-bgc-navy').removeClass('je-c-white');
						}
					});


					// Manages the attachments
					if ( jQuery('#attachmentmodule')[0] ) 
					{ 
						var domain = window.location.protocol + "//" + window.location.host + "/";
						var images = jQuery('#attachment_thumbnails li');

						if ( typeof images === "undefined" || images.length == 0 )
						{
							$wikiEdit.css('width', 'calc(100% - 1px)');
							$descriptionWikiEdit.css('width', 'calc(100% - 1px)');
							$saveOptions.css('margin-right', '0px');
							return;
						}

						if ( jQuery.isArray(images) || typeof images === "object" )
						{
							var imageArray = [];

							jQuery.each(images, function( key, val )
							{
								var imageUrl = jQuery(val).find('a.attachment-title').attr('href');
								imageArray.push( '<a class="je-a-i-' + key + '" href="' + domain + imageUrl + '" target="_blank"><img class="j-editor-image" src="' + domain + imageUrl + '" /></a>' );
							});

							var imageUrls = imageArray.join('');
							var imageData = '<div class="je-a-slider">' + imageUrls + '</div>';
						}
						else
						{
							var imageData = '<a href="' + domain + images + '" target="_blank"><img class="j-editor-image" src="' + domain + images + '" /></a>';
						}

						if ( !jQuery('.j-editor-attachments')[0] )
						{
							var imageViewer = '<div class="j-editor-attachments">' + imageData + '</div>';
							$wikiEdit.after( imageViewer );
						}
					}
					else
					{
						jQuery('.full-screen-editor .button.images').remove();
						$form.addClass('no-images');
					}

					// Manages the comments
					if ( typeof jQuery('.issue-data-block') === 'undefined' || jQuery('.issue-data-block').length == 0 )
					{
						jQuery('.full-screen-editor .button.comments').remove();
						$form.addClass('no-comments');
					}
					else
					{
						jQuery('li[data-id="comment-tabpanel"] a').click().delay(200);

						if ( !jQuery('.je-a-comments')[0] )
						{
							if ( !jQuery('.j-editor-attachments')[0] )
							{
								$wikiEdit.after( '<div class="j-editor-attachments" />' );
							}
							
							var comments = jQuery('.issuePanelContainer').html();
							jQuery('.j-editor-attachments').append('<div class="je-a-comments"><div class="je-comments-list">' + comments + '</div></div>');
							jQuery('#addcomment li a').click();

							var addComment = jQuery('#addcomment #addcomment').html();
							jQuery('.je-a-comments').append('<div class="je-comments-box">' + addComment + "</div>");
							jQuery('.je-comments-box textarea').attr('placeholder', 'Add your comment here...');
							jQuery('.je-a-comments').append('<div class="je-a-add-comments"></div>');
							jQuery('.je-comments-box .buttons a').remove();

							jQuery('.je-comments-box #issue-comment-add-submit').click(function()
							{
								jQuery('.je-comments-box form').submit();
							});
						}
					}
				});


				// Adds logic for modifying the style of the images button
				jQuery('.j-text-editor .button.images').click(function()
				{
					if ( jQuery(this).hasClass('selected') )
					{
						// Do nothing
						jQuery('.full-screen-editor .button.comments').removeClass('selected');
					}
					else
					{
						jQuery(this).addClass('selected');
						jQuery('.je-a-comments').addClass('je-hidden');
						jQuery('.full-screen-editor .button.comments').removeClass('selected');
					}
				});


				// Adds logic for modifying the style of the comments button
				jQuery('.j-text-editor .button.comments').click(function()
				{
					if ( jQuery(this).hasClass('selected') )
					{
						// Do nothing
						jQuery('.full-screen-editor .button.images').removeClass('selected');
					}
					else
					{
						jQuery(this).addClass('selected');
						jQuery('.je-a-comments').removeClass('je-hidden');
						jQuery('.full-screen-editor .button.images').removeClass('selected');
					}
				});


				// Adds logic for getting comments view
				jQuery('.j-text-editor .button.comments').click(function()
				{
					jQuery('li[data-id="comment-tabpanel"] a').click().delay(200);

					var comments = jQuery('.issuePanelContainer').html();

					if ( !jQuery('.je-a-comments')[0] )
					{
						jQuery('.j-editor-attachments').append('<div class="je-a-comments"><div class="je-comments-list">' + comments + '</div></div>');
						jQuery('#addcomment li a').click();

						var addComment = jQuery('#addcomment #addcomment').html();
						jQuery('.je-a-comments').append('<div class="je-comments-box">' + addComment + "</div>");
						jQuery('.je-comments-box textarea').attr('placeholder', 'Add your comment here...');
						jQuery('.je-a-comments').append('<div class="je-a-add-comments"></div>');
						jQuery('.je-comments-box .buttons a').remove();

						jQuery('.je-comments-box #issue-comment-add-submit').click(function()
						{
							jQuery('.je-comments-box form').submit();
						});
					}

				});
			}

			prependEditor( ta );
			buttonMgmt();
		}
	}

	addObserver();

});